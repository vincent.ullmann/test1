FROM ubuntu:18.04

RUN apt-get update -y
RUN apt-get install -y python-pip python-dev


##################
# SETUP FLASK
RUN pip install -U Flask

##################
# SETUP FLASK
RUN mkdir -p /app
COPY . /app
WORKDIR /app
# EXPOSE 5000:5000

#### MAIN
ENTRYPOINT python main.py

# RUN
# >>> docker run -d -p 5000:5000

